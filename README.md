At our engineering school, we always look forward to attending events in and out of our school 
but it seems to be really hard as we get to only hear about it way after the event took place due to the overwhelming amout of info posted online,
it's extremely difficult to filter out the events and sort them out according to date.
So, as engineering students from the Higher School of Communications of Tunis who went through this cycle multiple times,
we are willing to give you the chance to view all those events at one glance in our website and go as far as register directly from here so you won't miss our on any event of your choice!
This can help advertise all of your events as well for free!